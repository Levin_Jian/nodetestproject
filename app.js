var express = require('express')
        , stylus = require('stylus')
        , nib = require('nib');
var app = express();
function compile(str, path) {
    return stylus(str)
            .set('filename', path)
            .use(nib())
}
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(stylus.middleware(
        {src: __dirname + '/public'
                    , compile: compile
        }
));
app.use(express.static(__dirname + '/public'));
app.get('/', function(req, res) {
    res.render('index',
            {title: 'Home'});
});
app.get('/testjade', function(req, res) {
//    var jade = require('jade');
//    var filepath = __dirname + '/views/index.jade';
//    var options = {};
//    jade.renderFile(filepath, options, function(err, html) {
//        if (err)
//            throw err;
//        // ...
//        console.log(html);
//    });
//    var html = jade.renderFile(filepath, options);
//    console.log(html);
    var fs = require('fs');
    var filepath = __dirname + '/views/index.jade';
    fs.readFile(filepath, {encoding: 'utf8'}, function(err, data) {
        if (err)
            throw err;
        console.log(data);
        //here compile jade
        var jade = require('jade');
        var options = {filename:__dirname + '/views/index.jade'};
        jade.render(data, options, function(err, html) {
            if (err)
                throw err;
            // ...
            console.log(html);
            res.send(html);
        });
    });

});
app.listen(3000);



